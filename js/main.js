$(document).ready(function(){
	// init Isotope
	var $grid = $('.grid');

	$('.filter-button-group').on('click', 'button', function() {
		$("#alertModal").modal();
	});

	// initialize isotop when images loaded
	// avoid images overlapping
	$(window).load(function(){
		$grid.isotope({
		  // options
		  itemSelector: '.portfolio-item',
		  layoutMode: 'fitRows'
		});

		// remove previous onclick listener
		$('.filter-button-group').off('click');
		// filter items on button click
		$('.filter-button-group').on('click', 'button', function() {
			var filterValue = $(this).attr('data-filter');
			$grid.isotope({ filter: filterValue });
		});

		// trigger one click for all first, this enable animation
		$grid.isotope({ filter: "*" });
	});

	// init Magnific-Popup
	$('.img-popup').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		image: {
			verticalFit: true
		}, zoom: {
			enabled: true,
			duration: 300 // don't foget to change the duration also in CSS
		}
	});
	$('.img-popup-more').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		image: {
			verticalFit: true
		}
	});

	$('.hkgg-gallery').magnificPopup({
		type: 'image',
		items: [
		  {
			src: 'img/hkgg/hkgg1.jpg'
		  },
		  {
			src: 'img/hkgg/hkgg2.jpg'
		  },
		  {
			src: 'img/hkgg/hkgg3.jpg'
		  },
		  {
			src: 'img/hkgg/hkgg4.jpg'
		  },
		  {
			src: 'img/hkgg/hkgg5.jpg'
		  },
		  {
			src: 'img/hkgg/hkgg6.jpg'
		  },
		  {
			src: 'img/hkgg/hkgg7.jpg'
		  }
		],
		gallery: {
		  enabled: true
		}
	});

	$('.ywca-gallery').magnificPopup({
		type: 'image',
		items: [
		  {
			src: 'img/ywca/ywca1.png'
		  },
		  {
			src: 'img/ywca/ywca2.png'
		  },
			{
			src: 'img/ywca/ywca2-1.png'
		  },
			{
			src: 'img/ywca/ywca2-2.png'
		  },
		  {
			src: 'img/ywca/ywca3.png'
		  },
		  {
			src: 'img/ywca/ywca4.png'
		  },
		  {
			src: 'img/ywca/ywca5.png'
		  },
		  {
			src: 'img/ywca/ywca6.png'
		  },
			{
			src: 'img/ywca/ywca6-1.png'
		  },
		  {
			src: 'img/ywca/ywca7.png'
		  },
		  {
			src: 'img/ywca/ywca8.png'
		  },
		  {
			src: 'img/ywca/ywca9.png'
		  },
		  {
			src: 'img/ywca/ywca10.png'
		  },
		  {
			src: 'img/ywca/ywca11.png'
		  },
		  {
			src: 'img/ywca/ywca12.png'
			},
			{
			src: 'img/ywca/ywca13.png'
			},
			{
			src: 'img/ywca/ywca14.png'
			}
		],
		gallery: {
		  enabled: true
		}
	});

	$('.itsc-gallery').magnificPopup({
		type: 'image',
		items: [
		  {
			src: 'img/itsc/itsc1.JPG'
		  },
		  {
			src: 'img/itsc/itsc2.JPG'
		  },
		  {
			src: 'img/itsc/itsc3.JPG'
		  },
		  {
			src: 'img/itsc/itsc4.JPG'
		  },
		  {
			src: 'img/itsc/itsc5.JPG'
		  },
		  {
			src: 'img/itsc/itsc6.JPG'
		  },
		  {
			src: 'img/itsc/itsc7.JPG'
		  },
		  {
			src: 'img/itsc/itsc8.JPG'
		  },
		  {
			src: 'img/itsc/itsc9.JPG'
		  },
		  {
			src: 'img/itsc/itsc10.JPG'
		  }
		],
		gallery: {
		  enabled: true
		}
	});

});
